#!/usr//bin/python3.4

from imgurpython import ImgurClient
from urllib import request
import os

#Default values
home = '/home/user/'
client_id=''
client_secret=''
username=''
savedir=home+'ImgurFavourites'
successlog=home+'ImgurFavouritesScraperLog.success'
faillog=home+'ImgurFavouritesScraperLog.failed'
use_section=True
section=''
current=[]
fail=[]
confirm='n'

client=ImgurClient(client_id,client_secret)
while confirm=='n' or confirm=='no':
    print ("Running with settings ok?\nUsername: "+username+"\nSaveing to: "+savedir+"\nDividing in sections (WTF,Funny,Cats,...)"+str(use_section)+"\nLogging to: "+successlog)
    confirm=input("[Y/n]: ")
    if not(confirm=='n' or confirm=='no'):
        break
    username=input("username ("+username+"): ") or username
    savedir=input("savedir ("+savedir+"): ") or savedir
    successlog=input("Log ("+successlog+"): ") or succeslog
    faillog=successlog+username+'-Failed'
    use_section=input("Divide in sections -[leave blank to unset] ("+str(use_section)+") : ") and True
print("starting...")

#Setting up client
favourites=client.get_gallery_favorites(username)
#Checking if logs are existing -- url's in success log will not be downloaded
if os.path.exists(successlog):
    success = [line.rstrip('\n') for line in open(successlog)]
else:
    success=[]
#checking/creating savedir
if not os.path.exists(savedir):
    os.makedirs(savedir)

#Start Scraping
for fav in favourites:
    #check if album
    if hasattr(fav,'images_count'):
        print("Checking album "+fav.id)
        album=client.get_album_images(fav.id)
        #check if we're using sections, define section and create folder
        if use_section==True:
            section=str(fav.id)+'/'
            print("Putting in folder: "+str(fav.id))
            if not os.path.exists(savedir+'/'+section):
                os.makedirs(savedir+'/'+section)
        #for all images of album try to download
        for image in album:
            #check if already succesfull from log
            if image.link in success:
                print(image.link+" Already saved")
                continue;
            #name and download
            name=image.link.rsplit('/',1)[1]
            try:
                request.urlretrieve(image.link,savedir+'/'+section+name)
            except:
                print("error: Failed saving "+str(image.link)+" to "+savedir+'/'+section+name)
                fail.append(image.link)
                raise
            else:
                current.append(image.link)
    else:
        #Check if already succesfull from log
        if fav.link in success:
            print(fav.link+" Already saved")
            continue
        #name and download
        print("new favourite found! "+fav.title)
        name=fav.link.rsplit('/',1)[1]
        print("Getting image "+fav.id)
        if use_section==True:
            section=str(fav.section)+'/'
            if not os.path.exists(savedir+'/'+section):
                os.makedirs(savedir+'/'+section)
        try:
            request.urlretrieve(fav.link,savedir+'/'+section+name)
        except:
            print("error: Failed saving "+str(fav.link)+" to "+savedir+'/'+section+name)
            fail.append(fav.link)
        else:
            current.append(fav.link)
#logging
print("done\nWriting Logs") 
d=open(successlog,'a')
d.write('\n'.join(current))
d.close

f=open(faillog,'a')
f.write('\n'.join(fail))
f.close
#closing
print("done") 
print("Following files failed:")
print("\n".join(fail))

        
    
